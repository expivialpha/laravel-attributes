<?php

declare(strict_types=1);

namespace Expivi\Attributes\Events;

use Exception;
use Expivi\Attributes\Models\Value;
use Expivi\Attributes\Support\ValueCollection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Model as Entity;
use Illuminate\Support\Collection;

class EntityWasSaved
{
    /**
     * The trash collection.
     */
    protected Collection $trash;

    /**
     * Save values when an entity is saved.
     *
     * @throws Exception
     */
    public function handle(Entity $entity): void
    {
        $this->trash = $entity->getEntityAttributeValueTrash();

        // Wrap the whole process inside database transaction
        $connection = $entity->getConnection();
        $connection->beginTransaction();

        try {
            foreach ($entity->getEntityAttributes() as $attribute) {
                if ($entity->relationLoaded($relation = $attribute->getAttribute('slug'))) {
                    $relationValue = $entity->getRelationValue($relation);

                    if ($relationValue instanceof ValueCollection) {
                        foreach ($relationValue as $value) {
                            // Set attribute value's entity_id since it's always null,
                            // @TODO: because when RelationBuilder::build is called very early
                            $value->setAttribute('entity_id', $entity->getKey());
                            $this->saveOrTrashValue($value);
                        }
                    } elseif ($relationValue !== null) {
                        // Set attribute value's entity_id since it's always null,
                        // @TODO: because when RelationBuilder::build is called very early
                        $relationValue->setAttribute('entity_id', $entity->getKey());
                        $this->saveOrTrashValue($relationValue);
                    }
                }
            }

            if ($this->trash->count()) {
                $this->trash->mapToGroups(static function ($item) {
                    return [get_class($item) => $item];
                })->reduce(static function ($carry, $group) {
                    $trash = collect($group);
                    // Fetch the first item's class to know the model used for deletion
                    /** @var class-string<Model> $class */
                    $class = get_class($trash->first());

                    // Let's batch delete all the values based on their ids
                    return $carry && $class::whereIn('id', $trash->pluck('id'))->delete();
                }, true);

                // Now, empty the trash
                $this->trash = collect([]);
            }
        } catch (Exception $e) {
            // Rollback transaction on failure
            $connection->rollBack();

            throw $e;
        }

        // Commit transaction on success
        $connection->commit();
    }

    /**
     * Save or trash the given value according to it's content.
     */
    protected function saveOrTrashValue(Value $value): void
    {
        // In order to provide flexibility and let the values have their own
        // relationships, here we'll check if a value should be completely
        // saved with its relations or just save its own current state.
        if ($value !== null && !$this->trashValue($value)) {
            if ($value->shouldPush()) {
                $value->push();
            } else {
                $value->save();
            }
        }
    }

    /**
     * Trash the given value.
     */
    protected function trashValue(Value $value): bool
    {
        if ($value->getAttribute('content') !== null) {
            return false;
        }

        if ($value->exists) {
            // Push value to the trash
            $this->trash->push($value);
        }

        return true;
    }
}
