<?php

declare(strict_types=1);

namespace Expivi\Attributes\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model as Entity;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Support\Arr;

class EagerLoadScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     */
    public function apply(Builder $builder, Entity $entity): void
    {
        $eagerLoads = $builder->getEagerLoads();

        // If there is any eagerload matching the eav key, we will replace it with
        // all the registered properties for the entity. We'll simulate as if the
        // user has manually added all of these withs in purpose when querying.
        if (array_key_exists('eav', $eagerLoads)) {
            $eagerLoads = array_merge($eagerLoads, $entity->getEntityAttributeRelations());

            $builder->setEagerLoads(Arr::except($eagerLoads, 'eav'));
        }
    }
}
