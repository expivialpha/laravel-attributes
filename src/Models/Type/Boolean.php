<?php

declare(strict_types=1);

namespace Expivi\Attributes\Models\Type;

use Carbon\Carbon;
use Eloquent;
use Expivi\Attributes\Models\Attribute;
use Expivi\Attributes\Models\Value;
use Illuminate\Database\Eloquent\Model;

/**
 * Expivi\Attributes\Models\Type\Boolean.
 *
 * @property int            $id
 * @property bool           $content
 * @property int            $attribute_id
 * @property int            $entity_id
 * @property string         $entity_type
 * @property Carbon|null    $created_at
 * @property Carbon|null    $updated_at
 * @property Attribute      $attribute
 * @property Eloquent|Model $entity
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\Expivi\Attributes\Models\Type\Boolean whereAttributeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Expivi\Attributes\Models\Type\Boolean whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Expivi\Attributes\Models\Type\Boolean whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Expivi\Attributes\Models\Type\Boolean whereEntityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Expivi\Attributes\Models\Type\Boolean whereEntityType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Expivi\Attributes\Models\Type\Boolean whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Expivi\Attributes\Models\Type\Boolean whereUpdatedAt($value)
 *
 * @mixin \Eloquent
 */
class Boolean extends Value
{
    protected $casts = [
        'content' => 'boolean',
        'attribute_id' => 'integer',
        'entity_id' => 'integer',
        'entity_type' => 'string',
    ];

    /**
     * Create a new Eloquent model instance.
     */
    public function __construct(array $attributes = [])
    {
        $this->setTable(config('expivi.attributes.tables.attribute_boolean_values'));
        $this->mergeRules([
            'content' => 'required|boolean',
            'attribute_id' => 'required|integer|exists:'.config('expivi.attributes.tables.attributes').',id',
            'entity_id' => 'required|integer',
            'entity_type' => 'required|string|max:150',
        ]);

        parent::__construct($attributes);
    }
}
