<?php

declare(strict_types=1);

namespace Expivi\Attributes\Models\Type;

use Carbon\Carbon;
use Eloquent;
use Expivi\Attributes\Models\Attribute;
use Expivi\Attributes\Models\Value;
use Illuminate\Database\Eloquent\Model;

/**
 * Expivi\Attributes\Models\Type\Integer.
 *
 * @property int            $id
 * @property int            $content
 * @property int            $attribute_id
 * @property int            $entity_id
 * @property string         $entity_type
 * @property Carbon|null    $created_at
 * @property Carbon|null    $updated_at
 * @property Attribute      $attribute
 * @property Eloquent|Model $entity
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\Expivi\Attributes\Models\Type\Integer whereAttributeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Expivi\Attributes\Models\Type\Integer whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Expivi\Attributes\Models\Type\Integer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Expivi\Attributes\Models\Type\Integer whereEntityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Expivi\Attributes\Models\Type\Integer whereEntityType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Expivi\Attributes\Models\Type\Integer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Expivi\Attributes\Models\Type\Integer whereUpdatedAt($value)
 *
 * @mixin \Eloquent
 */
class Integer extends Value
{
    protected $casts = [
        'content' => 'integer',
        'attribute_id' => 'integer',
        'entity_id' => 'integer',
        'entity_type' => 'string',
    ];

    /**
     * Create a new Eloquent model instance.
     */
    public function __construct(array $attributes = [])
    {
        $this->setTable(config('expivi.attributes.tables.attribute_integer_values'));
        $this->mergeRules([
            'content' => 'required|integer',
            'attribute_id' => 'required|integer|exists:'.config('expivi.attributes.tables.attributes').',id',
            'entity_id' => 'required|integer',
            'entity_type' => 'required|string|max:150',
        ]);

        parent::__construct($attributes);
    }
}
