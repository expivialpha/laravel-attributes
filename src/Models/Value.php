<?php

declare(strict_types=1);

namespace Expivi\Attributes\Models;

use Expivi\Attributes\Support\ValueCollection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Rinvex\Support\Traits\ValidatingTrait;

abstract class Value extends Model
{
    use ValidatingTrait;

    protected $fillable = [
        'content',
        'attribute_id',
        'entity_id',
        'entity_type',
    ];

    /**
     * Determine if value should push to relations when saving.
     */
    protected bool $shouldPush = false;

    /**
     * The default rules that the model will validate against.
     */
    protected array $rules = [];

    /**
     * Whether the model should throw a
     * ValidationException if it fails validation.
     */
    protected bool $throwValidationExceptions = true;

    /**
     * Relationship to the attribute entity.
     */
    public function attribute(): BelongsTo
    {
        return $this->belongsTo(Attribute::class, 'attribute_id', 'id', 'attribute');
    }

    /**
     * Polymorphic relationship to the entity instance.
     */
    public function entity(): MorphTo
    {
        return $this->morphTo('entity', 'entity_type', 'entity_id', 'id');
    }

    /**
     * Check if value should push to relations when saving.
     */
    public function shouldPush(): bool
    {
        return $this->shouldPush;
    }

    public function newCollection(array $models = [])
    {
        return new ValueCollection($models);
    }
}
