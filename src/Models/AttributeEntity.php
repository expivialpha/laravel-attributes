<?php

declare(strict_types=1);

namespace Expivi\Attributes\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Expivi\Attributes\Models\AttributeEntity.
 *
 * @property int         $attribute_id
 * @property string      $entity_type
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\Expivi\Attributes\Models\AttributeEntity whereAttributeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Expivi\Attributes\Models\AttributeEntity whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Expivi\Attributes\Models\AttributeEntity whereEntityType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Expivi\Attributes\Models\AttributeEntity whereUpdatedAt($value)
 *
 * @mixin \Eloquent
 */
class AttributeEntity extends Model
{
    protected $fillable = [
        'entity_type',
    ];

    protected $casts = [
        'entity_type' => 'string',
    ];

    /**
     * Create a new Eloquent model instance.
     */
    public function __construct(array $attributes = [])
    {
        $this->setTable(config('expivi.attributes.tables.attribute_entity'));

        parent::__construct($attributes);
    }
}
