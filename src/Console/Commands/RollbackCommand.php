<?php

declare(strict_types=1);

namespace Expivi\Attributes\Console\Commands;

use Illuminate\Console\Command;

class RollbackCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'expivi:rollback:attributes {--f|force : Force the operation to run when in production.}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Rollback Expivi Attributes Tables.';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $this->alert($this->description);

        $path = config('expivi.attributes.autoload_migrations') ?
            'vendor/expivi/laravel-attributes/database/migrations' :
            'database/migrations/expivi/laravel-attributes';

        if (file_exists($path)) {
            $this->call('migrate:reset', [
                '--path' => $path,
                '--force' => $this->option('force'),
            ]);
        } else {
            $this->warn('No migrations found! Consider publish them first: <fg=green>php artisan expivi:publish:attributes</>');
        }

        $this->line('');
    }
}
