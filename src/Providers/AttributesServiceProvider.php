<?php

declare(strict_types=1);

namespace Expivi\Attributes\Providers;

use Expivi\Attributes\Console\Commands\MigrateCommand;
use Expivi\Attributes\Console\Commands\PublishCommand;
use Expivi\Attributes\Console\Commands\RollbackCommand;
use Expivi\Attributes\Models\Attribute;
use Expivi\Attributes\Models\AttributeEntity;
use Illuminate\Support\ServiceProvider;
use Rinvex\Support\Traits\ConsoleTools;

class AttributesServiceProvider extends ServiceProvider
{
    use ConsoleTools;

    /**
     * The commands to be registered.
     */
    protected array $commands = [
        MigrateCommand::class,
        PublishCommand::class,
        RollbackCommand::class,
    ];

    public function register()
    {
        // Merge config
        $this->mergeConfigFrom(dirname(__DIR__, 2).'/config/config.php', 'expivi.attributes');

        // Bind eloquent models to IoC container
        $this->registerModels([
            'expivi.attributes.attribute' => Attribute::class,
            'expivi.attributes.attribute_entity' => AttributeEntity::class,
        ]);

        // Register attributes entities
        $this->app->singleton('expivi.attributes.entities', static function ($app) {
            return collect();
        });

        // Register console commands
        $this->commands($this->commands);
    }

    public function boot()
    {
        $this->publishes([
            dirname(__DIR__, 2).'/config/config.php' => config_path('expivi.attributes.php'),
        ], 'expivi-attributes-config');

        $this->publishes([
            dirname(__DIR__, 2).'/database/migrations' => database_path('migrations'),
        ], 'expivi-attributes-migrations');

        if (config('expivi.attributes.autoload_migrations')) {
            $this->loadMigrationsFrom(dirname(__DIR__, 2).'/database/migrations');
        }
    }
}
