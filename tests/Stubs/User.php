<?php

declare(strict_types=1);

namespace Expivi\Attributes\Tests\Stubs;

use Expivi\Attributes\Traits\Attributable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    use Attributable;
    use HasFactory;
}
