<?php

declare(strict_types=1);

namespace Expivi\Attributes\Tests\Feature;

use Expivi\Attributes\Models\Attribute;
use Expivi\Attributes\Models\Type\Boolean;
use Expivi\Attributes\Models\Type\Datetime;
use Expivi\Attributes\Models\Type\Integer;
use Expivi\Attributes\Models\Type\Text;
use Expivi\Attributes\Models\Type\Varchar;
use Expivi\Attributes\Providers\AttributesServiceProvider;
use Expivi\Attributes\Tests\Stubs\User;

class TestCase extends \Orchestra\Testbench\TestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        $this->artisan('migrate', ['--database' => 'testing']);
        $this->loadLaravelMigrations('testing');

        // Registering the core type map
        Attribute::typeMap([
            'text' => Text::class,
            'bool' => Boolean::class,
            'integer' => Integer::class,
            'varchar' => Varchar::class,
            'datetime' => Datetime::class,
        ]);

        // Push your entity fully qualified namespace
        app('expivi.attributes.entities')->push(User::class);
    }

    protected function getEnvironmentSetUp($app)
    {
        $app['config']->set('database.default', 'testing');
        $app['config']->set('database.connections.testing', [
            'driver' => 'sqlite',
            'database' => ':memory:',
            'prefix' => '',
        ]);
    }

    protected function getPackageProviders($app)
    {
        return [
            AttributesServiceProvider::class,
        ];
    }
}
