<?php

declare(strict_types=1);

namespace Expivi\Attributes\Tests\Feature;

use Expivi\Attributes\Tests\Stubs\User;

class AttributeCreationTest extends TestCase
{
    public function test_it_creates_a_new_attribute()
    {
        $attribute = $this->createAttribute();

        $this->assertDatabaseHas(config('expivi.attributes.tables.attributes'), ['slug' => 'count', 'type' => 'integer']);
        $this->assertDatabaseHas(config('expivi.attributes.tables.attribute_entity'), ['attribute_id' => $attribute->id, 'entity_type' => User::class]);
    }

    public function test_it_ensures_snake_case_slugs()
    {
        $attribute = $this->createAttribute(['name' => 'Foo Bar']);

        $this->assertEquals('foo_bar', $attribute->slug);
        $this->assertDatabaseHas(config('expivi.attributes.tables.attributes'), ['slug' => 'foo_bar', 'type' => 'integer']);
    }

    public function test_it_ensures_snake_case_slugs_even_if_dashed_slugs_provided()
    {
        $attribute = $this->createAttribute(['slug' => 'foo-bar']);

        $this->assertEquals('foo_bar', $attribute->slug);
    }

    public function test_it_ensures_unique_slugs()
    {
        $this->createAttribute(['name' => 'foo']);
        $this->createAttribute(['name' => 'foo']);

        $this->assertDatabaseHas(config('expivi.attributes.tables.attributes'), ['slug' => 'foo_1']);
    }

    public function test_it_ensures_unique_slugs_even_if_slugs_explicitly_provided()
    {
        $this->createAttribute(['slug' => 'foo']);
        $this->createAttribute(['slug' => 'foo']);

        $this->assertDatabaseHas(config('expivi.attributes.tables.attributes'), ['slug' => 'foo_1']);
    }

    protected function createAttribute($attributes = [])
    {
        return app('expivi.attributes.attribute')->create(array_merge([
            'type' => 'integer',
            'name' => 'Count',
            'entities' => [User::class],
        ], $attributes));
    }
}
