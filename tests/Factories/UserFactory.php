<?php

declare(strict_types=1);

namespace Database\Factories\Expivi\Attributes\Tests\Stubs;

use Expivi\Attributes\Tests\Stubs\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserFactory extends Factory
{
    protected $model = User::class;

    public function definition(): array
    {
        return [
            'name' => fake()->name,
            'email' => fake()->email,
            'password' => 'foobarbaz',
        ];
    }
}
