<?php

declare(strict_types=1);
use Expivi\Attributes\Models\Attribute;
use Expivi\Attributes\Models\AttributeEntity;

return [
    // Manage autoload migrations
    'autoload_migrations' => true,

    // Attributes Database Tables
    'tables' => [
        'attributes' => 'eav_attributes',
        'attribute_entity' => 'eav_attribute_entity',
        'attribute_boolean_values' => 'eav_attribute_boolean_values',
        'attribute_datetime_values' => 'eav_attribute_datetime_values',
        'attribute_integer_values' => 'eav_attribute_integer_values',
        'attribute_text_values' => 'eav_attribute_text_values',
        'attribute_varchar_values' => 'eav_attribute_varchar_values',
        'attribute_json_values' => 'eav_attribute_json_values',
    ],

    // Attributes Models
    'models' => [
        'attribute' => Attribute::class,
        'attribute_entity' => AttributeEntity::class,
    ],
];
