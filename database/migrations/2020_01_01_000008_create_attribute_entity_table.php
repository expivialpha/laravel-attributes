<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttributeEntityTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create(config('expivi.attributes.tables.attribute_entity'), static function (Blueprint $table) {
            // Columns
            $table->integer('attribute_id')->unsigned();
            $table->string('entity_type');
            $table->integer('entity_id')->unsigned()->nullable(); // TODO: Making this nullable for now as it breaks the basic features
            $table->timestamps();

            // Indexes
            $table->unique(['attribute_id', 'entity_type'], 'attributable_attribute_id_entity_type');
            $table->foreign('attribute_id')->references('id')->on(config('expivi.attributes.tables.attributes'))
                  ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists(config('expivi.attributes.tables.attribute_entity'));
    }
}
