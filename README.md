# Expivi Attributes

**Expivi Attributes** is a rewritten fork of Rinvex Attributes which is a robust, intelligent, and integrated Entity-Attribute-Value model (EAV) implementation for Laravel Eloquent, with powerful underlying for managing entity attributes implicitly as relations with ease. It utilizes the power of Laravel Eloquent, with smooth and seamless integration.

## Credits notice

This package is a rewritten fork of [Rinvex Attributes](https://packagist.org/packages/rinvex/laravel-attributes#v2.1.1)

