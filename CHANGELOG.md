# Expivi Attributes Change Log

All notable changes to this project will be documented in this file.

This project adheres to [Semantic Versioning](CONTRIBUTING.md).

## [v6.1.0] - 2022-02-14
- Update composer dependencies to Laravel v9
- FIX trash multiple attribute types in one save (#101)
- Fix all attribute values of type varchar documentation example (fixes #155)
- Drop PHP v7 support, and upgrade rinvex package dependencies to next major version
- Merge rules instead of resetting, to allow adequate model override
- Update composer dependencies
- Drop old MySQL versions support that doesn't support json columns
- Fix constructor initialization order (fill attributes should come next after merging fillables & rules)
- Upgrade to GitHub-native Dependabot
- Simplify Attributable trait
- Simplify service provider model registration into IoC
- Change hardcoded AttributeEntity table name into dynamically config option (fix #77)
- Enable StyleCI risky mode

## [v5.0.1] - 2020-12-25
- Add support for PHP v8
- Upgrade to Laravel v8
- Update validation rules

## [v4.1.0] - 2020-06-15
- Update validation rules
- Drop using rinvex/laravel-cacheable from core packages for more flexibility
    - Caching should be handled on the application layer, and not enforced from the core packages
- Drop PHP 7.2 & 7.3 support from travis
- Drop slugifying group attribute
- Remove default indent size config
- Add strip_tags validation rule to string fields
- Specify events queue
- Explicitly specify relationship attributes
- Add strip_tags validation rule
- Explicitly define relationship name
- Fix ServiceProvider registerCommands method compatibility
- Tweak artisan command registration
- Reverse commit "Convert database int fields into bigInteger"
- Refactor publish command and allow multiple resource values
- Fix namespace issue
- Enforce consistent artisan command tag namespacing
- Enforce consistent package namespace
- Drop laravel/helpers usage as it's no longer used
- Convert into bigInteger database fields
- Add shortcut -f (force) for artisan publish commands
- Fix migrations path
- Upgrade to Laravel v7.1.x & PHP v7.4.x

## [v3.0.3] - 2020-07-01
- ReForked from https://packagist.org/packages/rinvex/laravel-attributes#v3.0.3
- Significant performance improvement by bypassing Eloquent model magic when attaching attribute relationships to models.
- Tweak TravisCI config
- Add migrations autoload option to the package
- Tweak service provider `publishesResources`
- Remove indirect composer dependency
- Drop using global helpers
- Update StyleCI config
- Fix wrong code sample in readme
- Fix `migrate:reset` args as it doesn't accept --step
- Add missing laravel/helpers composer package
- Upgrade to Laravel v6 and update dependencies

## [v2.1.1] - 2020-06-10
- Forked from https://packagist.org/packages/rinvex/laravel-attributes#v2.1.1
